﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    float scrollWheelSensitivity = 100;
    float panSensitivity = 30f;
    float rotation, rotationDuration = 2f, timeElapsed;
    float turnRotation = 90;

    Vector3 mouseOrigin = new Vector3();

    void Start()
    {
        rotation = 0;
        timeElapsed = 0;
    }

    void Update()
    {
        //ROTATION
        if(timeElapsed < rotationDuration)
        {
            //Animating rotation
            transform.rotation = Quaternion.Lerp(
                transform.rotation, 
                Quaternion.Euler(new Vector3(66.8f, rotation, 0f)), 
                timeElapsed / rotationDuration);

            timeElapsed += Time.deltaTime;
        }
        else
        {
            //snap to final destination/rotation
            transform.rotation = Quaternion.Euler(new Vector3(66.8f, rotation, 0f));
        }


        if (Input.GetKeyUp(KeyCode.A))
        {
            rotation += -turnRotation;
            timeElapsed = 0;
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            rotation += turnRotation;
            timeElapsed = 0;
        }

        //ZOOM
        float scrollWheelInput = Input.GetAxis("Mouse ScrollWheel");
        if(scrollWheelInput != 0)
        {
            float normalize = scrollWheelInput * 10;
            transform.position = Vector3.Lerp(
                transform.position, 
                transform.position + Vector3.up * -normalize * scrollWheelSensitivity, 
                Time.deltaTime
                );
        }


        //PAN
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        //Debug.Log("mouseX: " + mouseX + " mouseY: " + mouseY);

        if (Input.GetKeyDown(KeyCode.Mouse2)) //middle mouse
        {
            mouseOrigin = Input.mousePosition;
            transform.rotation = Quaternion.Euler(new Vector3(66.8f, rotation, 0f));
            //Debug.Log("mouse origin: " + mouseOrigin);
        }

        if(Input.GetKey(KeyCode.Mouse2)) //middle mouse
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

            Vector3 move = new Vector3(pos.x * panSensitivity, 0, pos.y * panSensitivity);

            switch (transform.rotation.eulerAngles.y)
            {
                case 270:
                   // Debug.Log("rotation: 270");
                    move = new Vector3(pos.y * -panSensitivity, 0, pos.x * panSensitivity);
                    break;

                case 180:
                   // Debug.Log("rotation: 180");
                    move = new Vector3(pos.x * -panSensitivity, 0, pos.y * -panSensitivity);
                    break;

                case 90:
                  //  Debug.Log("rotation: 90");
                    move = new Vector3(pos.y * panSensitivity, 0, pos.x * -panSensitivity);
                    break;
            }

            transform.position = Vector3.Lerp(transform.position, transform.position + move, Time.deltaTime);
        }

    }
}
