﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRoute : MonoBehaviour
{
    [SerializeField]
    Transform[] route1ControlPoints;

    [SerializeField]
    Transform[] route2ControlPoints;

    [SerializeField]
    Transform[] route3ControlPoints;

    [SerializeField]
    Transform[] route4ControlPoints;

    private Vector3 gizmosPosition;

    private void OnDrawGizmos()
    {

        //ROUTE 1
        if (route1ControlPoints.Length == 4)
        {
            for (float t = 0; t <= 1; t += 0.05f)
            {
                gizmosPosition = Mathf.Pow(1 - t, 3) * route1ControlPoints[0].position
                    + 3 * Mathf.Pow(1 - t, 2) * t * route1ControlPoints[1].position
                    + 3 * (1 - t) * Mathf.Pow(t, 2) * route1ControlPoints[2].position
                    + Mathf.Pow(t, 3) * route1ControlPoints[3].position;

                Gizmos.DrawSphere(gizmosPosition, 0.25f);
            }

            Gizmos.DrawLine(new Vector3(route1ControlPoints[0].position.x, route1ControlPoints[0].position.y, route1ControlPoints[0].position.z),
                new Vector3(route1ControlPoints[1].position.x, route1ControlPoints[1].position.y, route1ControlPoints[1].position.z));

            Gizmos.DrawLine(new Vector3(route1ControlPoints[2].position.x, route1ControlPoints[2].position.y, route1ControlPoints[2].position.z),
                new Vector3(route1ControlPoints[3].position.x, route1ControlPoints[3].position.y, route1ControlPoints[3].position.z));
        }


        //ROUTE 2
        if (route2ControlPoints.Length == 4)
        {
            for (float t = 0; t <= 1; t += 0.05f)
            {
                gizmosPosition = Mathf.Pow(1 - t, 3) * route2ControlPoints[0].position
                    + 3 * Mathf.Pow(1 - t, 2) * t * route2ControlPoints[1].position
                    + 3 * (1 - t) * Mathf.Pow(t, 2) * route2ControlPoints[2].position
                    + Mathf.Pow(t, 3) * route2ControlPoints[3].position;

                Gizmos.DrawSphere(gizmosPosition, 0.25f);
            }

            Gizmos.DrawLine(new Vector3(route2ControlPoints[0].position.x, route2ControlPoints[0].position.y, route2ControlPoints[0].position.z),
                new Vector3(route2ControlPoints[1].position.x, route2ControlPoints[1].position.y, route2ControlPoints[1].position.z));

            Gizmos.DrawLine(new Vector3(route2ControlPoints[2].position.x, route2ControlPoints[2].position.y, route2ControlPoints[2].position.z),
                new Vector3(route2ControlPoints[3].position.x, route2ControlPoints[3].position.y, route2ControlPoints[3].position.z));
        }


        //ROUTE 3
        if (route3ControlPoints.Length == 4)
        {
            for (float t = 0; t <= 1; t += 0.05f)
            {
                gizmosPosition = Mathf.Pow(1 - t, 3) * route3ControlPoints[0].position
                    + 3 * Mathf.Pow(1 - t, 2) * t * route3ControlPoints[1].position
                    + 3 * (1 - t) * Mathf.Pow(t, 2) * route3ControlPoints[2].position
                    + Mathf.Pow(t, 3) * route3ControlPoints[3].position;

                Gizmos.DrawSphere(gizmosPosition, 0.25f);
            }

            Gizmos.DrawLine(new Vector3(route3ControlPoints[0].position.x, route3ControlPoints[0].position.y, route3ControlPoints[0].position.z),
                new Vector3(route3ControlPoints[1].position.x, route3ControlPoints[1].position.y, route3ControlPoints[1].position.z));

            Gizmos.DrawLine(new Vector3(route3ControlPoints[2].position.x, route3ControlPoints[2].position.y, route3ControlPoints[2].position.z),
                new Vector3(route3ControlPoints[3].position.x, route3ControlPoints[3].position.y, route3ControlPoints[3].position.z));
        }


        //ROUTE 4
        if(route4ControlPoints.Length == 4)
        {
            for (float t = 0; t <= 1; t += 0.05f)
            {
                gizmosPosition = Mathf.Pow(1 - t, 3) * route4ControlPoints[0].position
                    + 3 * Mathf.Pow(1 - t, 2) * t * route4ControlPoints[1].position
                    + 3 * (1 - t) * Mathf.Pow(t, 2) * route4ControlPoints[2].position
                    + Mathf.Pow(t, 3) * route4ControlPoints[3].position;

                Gizmos.DrawSphere(gizmosPosition, 0.25f);
            }

            Gizmos.DrawLine(new Vector3(route4ControlPoints[0].position.x, route4ControlPoints[0].position.y, route4ControlPoints[0].position.z),
                new Vector3(route4ControlPoints[1].position.x, route4ControlPoints[1].position.y, route4ControlPoints[1].position.z));

            Gizmos.DrawLine(new Vector3(route4ControlPoints[2].position.x, route4ControlPoints[2].position.y, route4ControlPoints[2].position.z),
                new Vector3(route4ControlPoints[3].position.x, route4ControlPoints[3].position.y, route4ControlPoints[3].position.z));
        }
        
    }
}
