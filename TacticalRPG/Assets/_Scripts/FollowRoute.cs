﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowRoute : MonoBehaviour
{
    [SerializeField]
    private Transform[] routes;

    private int routeToGo;

    private float tParam;

    private Vector3 cameraPosition;

    private float speedModifier, rotation;

    private bool coroutineAllowed;

    // Start is called before the first frame update
    void Start()
    {
        routeToGo = 0;
        tParam = 0f;
        speedModifier = 5f;
        rotation = 0;
        coroutineAllowed = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (coroutineAllowed)
        {
            if (Input.GetKeyUp("space"))
            {
                StartCoroutine(GoByTheRoute(routeToGo));
            }
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(66.8f, rotation, 0f)), tParam);
    }

    private IEnumerator GoByTheRoute(int routeNumber)
    {
        coroutineAllowed = false;

        switch (routeNumber)
        {
            case 0:
                rotation = 270;
                break;

            case 1:
                rotation = 180;
                break;

            case 2:
                rotation = 90;
                break;

            case 3:
                rotation = 0;
                break;
        }

        Vector3 p0 = routes[routeNumber].GetChild(0).position;
        Vector3 p1 = routes[routeNumber].GetChild(1).position;
        Vector3 p2 = routes[routeNumber].GetChild(2).position;
        Vector3 p3 = routes[routeNumber].GetChild(3).position;

        while (tParam < 1)
        {
            tParam += Time.deltaTime * speedModifier;

            cameraPosition = Mathf.Pow(1 - tParam, 3) * p0
                + 3 * Mathf.Pow(1 - tParam, 2) * tParam * p1
                + 3 * (1 - tParam) * Mathf.Pow(tParam, 2) * p2
                + Mathf.Pow(tParam, 3) * p3;

            transform.position = cameraPosition;

            yield return new WaitForEndOfFrame();
        }


        tParam = 0f;

        routeToGo += 1;

        if (routeToGo > routes.Length - 1)
        {
            routeToGo = 0;
        }

        coroutineAllowed = true;
    }
}
