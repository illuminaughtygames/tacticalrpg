﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    static GridBehavior _gridBehavior;

    static Player
        _playerController,
        _enemyController;

    static GameObject
        userActionMenu,
        openingMenu,
        enemyPhaseTransitionMenu,
        playerPhaseTransitionMenu,
        mainCamera;  

    private enum GameState
    {
        NOT_STARTED,
        TRANSITIONING_TO_PLAYER_TURN,
        PLAYER_FIRST_ACTION_DECIDING,
        PLAYER_FIRST_ACTION_EXECUTING,
        PLAYER_SECOND_ACTION_DECIDING,
        PLAYER_ATTACKING,
        PLAYER_USING_ITEM,
        OPPONENT_DEFENDING,
        OPPONENT_FIRST_ACTION_DECIDING,
        OPPONENT_FIRST_ACTION_EXECUTING,
        OPPONENT_SECOND_ACTION_DECIDING,
        TRANSITIONING_TO_OPPONENT_TURN
    };

    static GameState _currentGameState;

    public enum GameEvent
    {
        START_GAME,
        NODE_CLICKED,
        ACTION_DONE,
        ACTION_MENU,
        ATTACK,
        USE_ITEM,
        END_TURN,
        UNDO,
        START_TURN
    }
        

    void Start()
    {
        userActionMenu = gameObject.transform.Find("UserActionMenu").gameObject;
        openingMenu = gameObject.transform.Find("OpeningMenu").gameObject;
        openingMenu.SetActive(true);
        enemyPhaseTransitionMenu = gameObject.transform.Find("EnemyPhaseTransitionMenu").gameObject;
        playerPhaseTransitionMenu = gameObject.transform.Find("PlayerPhaseTransitionMenu").gameObject;

        _gridBehavior = gameObject.transform.Find("GridGenerator").gameObject.GetComponent<GridBehavior>();
        mainCamera = gameObject.transform.Find("Main Camera").gameObject;

        _playerController = gameObject.transform.Find("SquirrelBoy").gameObject.GetComponent<Player>();
        _playerController.currentNode = _gridBehavior.gridArray[0, 0].GetComponent<Node>(); //TODO: change to active player spawn node;
        _playerController.lastNode = _playerController.currentNode;

        _enemyController = gameObject.transform.Find("Ai_SquirrelBoy").gameObject.GetComponent<Player>();
        _enemyController.currentNode = _gridBehavior.gridArray[2, 1].GetComponent<Node>();
        _enemyController.lastNode = _enemyController.currentNode;
    }


    public void StartGame()
    {
        ProcessEvent(GameEvent.START_GAME, null);
    }

    public void StartTurn()
    {
        ProcessEvent(GameEvent.START_TURN, null);
    }


    public static void ProcessEvent(GameEvent eventType, GameObject eventObj)
    {
        switch (_currentGameState)
        {
            case GameState.NOT_STARTED:

                switch (eventType)
                {
                    case GameEvent.START_GAME:

                        openingMenu.SetActive(false);
                        playerPhaseTransitionMenu.SetActive(true);
                        _currentGameState = GameState.TRANSITIONING_TO_PLAYER_TURN;
                        break;
                }
                break;

            case GameState.TRANSITIONING_TO_PLAYER_TURN:

                switch (eventType)
                {
                    case GameEvent.START_TURN:

                        playerPhaseTransitionMenu.SetActive(false);
                        _currentGameState = GameState.PLAYER_FIRST_ACTION_DECIDING;
                        break;
                }

                break;

            case GameState.PLAYER_FIRST_ACTION_DECIDING:
                Debug.Log("GameState.PLAYER_FIRST_ACTION_DECIDING");

                switch (eventType)
                {
                    case GameEvent.NODE_CLICKED: //action = player move or player selected
                        Debug.Log("GameEvent.NODE_CLICKED");

                        _gridBehavior.SetDestination(
                            _playerController.currentNode.GetComponent<Node>().x,
                            _playerController.currentNode.GetComponent<Node>().y,
                            eventObj.GetComponent<Node>().x,
                            eventObj.GetComponent<Node>().y,
                            -1
                            );

                        
                        _playerController.SetPath(_gridBehavior.GetPath());

                        //place current active player onto node
                        _playerController.currentNode = eventObj.GetComponent<Node>();

                        _currentGameState = GameState.PLAYER_FIRST_ACTION_EXECUTING;
                        break;

                }
                break;

            case GameState.PLAYER_FIRST_ACTION_EXECUTING:

                switch (eventType)
                {
                    case GameEvent.ACTION_DONE:

                        userActionMenu.SetActive(true);
                        userActionMenu.GetComponent<UserActionMenu>()
                            .CanAttack(
                                _gridBehavior.GetEnemiesInAttackRange(
                                    _playerController.currentNode, 
                                    _enemyController.currentNode
                                    )
                            );

                        _currentGameState = GameState.PLAYER_SECOND_ACTION_DECIDING;
                        break;
                }

                break;

            case GameState.PLAYER_SECOND_ACTION_DECIDING:

                switch (eventType)
                {
                    case GameEvent.ATTACK:
                        
                        _playerController.gameObject.GetComponent<Animator>().SetTrigger("attack");

                        _currentGameState = GameState.PLAYER_ATTACKING;
                        break;

                    case GameEvent.USE_ITEM:

                        _playerController.gameObject.GetComponent<Animator>().SetTrigger("attack");

                        _currentGameState = GameState.PLAYER_USING_ITEM;
                        break;

                    case GameEvent.UNDO:

                        //resets the new start pos on grid to be at origin
                        _gridBehavior.SetDestination(
                            _playerController.currentNode.x,
                            _playerController.currentNode.y,
                            _playerController.lastNode.x,
                            _playerController.lastNode.y,
                            -1
                            );

                        //move player to origin node
                        _playerController.MoveToPosition(
                                new Vector3(
                                        _playerController.lastNode.x + GridBehavior.GRID_OFFSET,
                                        0,
                                        _playerController.lastNode.y + GridBehavior.GRID_OFFSET
                                    )
                            );

                        _playerController.currentNode = _playerController.lastNode;

                        _currentGameState = GameState.PLAYER_FIRST_ACTION_DECIDING;
                        break;

                    case GameEvent.END_TURN:
                        enemyPhaseTransitionMenu.SetActive(true);

                        _currentGameState = GameState.TRANSITIONING_TO_OPPONENT_TURN;
                        break;
                }

                break;

            case GameState.PLAYER_USING_ITEM:

                switch (eventType)
                {
                    case GameEvent.ACTION_DONE:
                        enemyPhaseTransitionMenu.SetActive(true);

                        _currentGameState = GameState.TRANSITIONING_TO_OPPONENT_TURN;
                        break;
                }

                break;

            case GameState.PLAYER_ATTACKING:

                switch (eventType)
                {
                    case GameEvent.ACTION_DONE:

                        _enemyController.GetComponent<Animator>().SetTrigger("attack");

                        _currentGameState = GameState.OPPONENT_DEFENDING;
                        break;
                }

                break;

            case GameState.OPPONENT_DEFENDING:

                switch (eventType)
                {
                    case GameEvent.ACTION_DONE:

                        enemyPhaseTransitionMenu.SetActive(true);

                        _currentGameState = GameState.TRANSITIONING_TO_OPPONENT_TURN;
                        break;
                }

                break;

            case GameState.TRANSITIONING_TO_OPPONENT_TURN:

                switch (eventType)
                {
                    case GameEvent.START_TURN:

                        enemyPhaseTransitionMenu.SetActive(false);
                        _enemyController.DecideMove();

                        _currentGameState = GameState.OPPONENT_FIRST_ACTION_DECIDING;
                        break;
                }

                break;

            case GameState.OPPONENT_FIRST_ACTION_DECIDING:
                Debug.Log("GameState.OPPONENT_FIRST_ACTION_DECIDING");

                switch (eventType)
                {
                    case GameEvent.NODE_CLICKED:
                        Debug.Log("GameState.OPPONENT_FIRST_ACTION_DECIDING -> GameEvent.AI_MOVE");

                        int
                            destinationX = _playerController.currentNode.x,
                            destinationY = _playerController.currentNode.y;

                        _gridBehavior.SetDestination(
                            _enemyController.currentNode.x,
                            _enemyController.currentNode.y, 
                            destinationX, 
                            destinationY,
                            _enemyController.movementSpeed
                            );
                        _enemyController.SetPath(_gridBehavior.GetPath());

                        _currentGameState = GameState.OPPONENT_FIRST_ACTION_EXECUTING;
              
                        break;
                }

                break;

            case GameState.OPPONENT_FIRST_ACTION_EXECUTING:

                switch (eventType)
                {
                    case GameEvent.ACTION_DONE:

                        _enemyController.currentNode = eventObj.GetComponent<Node>();
                        _enemyController.DecideEndTurn();

                        _currentGameState = GameState.OPPONENT_SECOND_ACTION_DECIDING;
                        break;
                }

                break;

            case GameState.OPPONENT_SECOND_ACTION_DECIDING:

                switch (eventType)
                {
                    case GameEvent.END_TURN:

                        playerPhaseTransitionMenu.SetActive(true);
                        _currentGameState = GameState.TRANSITIONING_TO_PLAYER_TURN;
                        break;
                }

                break;
        }
    }
}
