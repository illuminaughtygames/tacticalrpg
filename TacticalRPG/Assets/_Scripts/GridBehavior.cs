﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GridBehavior : MonoBehaviour
{
    

    public bool findDistance = false;
    public int rows = 10;
    public int columns = 10;
    public int scale = 1;
    public const float GRID_OFFSET = 0.5f;
    public GameObject gridPrefab;
    public Vector3 leftBottomLocation = new Vector3(0,0,0);
    public GameObject[,] gridArray;
    public int _startX = 0;
    public int _startY = 0;
    public int _endX = 2;
    public int _endY = 2;
    public int _movementRange = -1;
    public List<GameObject> _path = new List<GameObject>();

    // Start is called before the first frame update
    void Awake()
    {
        gridArray = new GameObject[columns, rows];

        if (gridPrefab)
        {
            GenerateGrid();
        }
        else
        {
            print("missing gridPrefab, please assign.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (findDistance)
        {
            SetDistance();
            SetPath(_movementRange);
            findDistance = false;
        }
                  
    }


    public void SetDestination(int startX, int startY, int endX, int endY, int movementRange)
    {
        _path.Clear();
        _startX = startX;
        _startY = startY;
        _endX = endX;
        _endY = endY;

        _movementRange = movementRange;
        findDistance = true;
    }


    void GenerateGrid()
    {
        for(int i = 0; i < columns; i++)
        {
            for(int k = 0; k < rows; k++)
            {
                Vector3 spawnPos = new Vector3(
                    leftBottomLocation.x + scale * i + GRID_OFFSET, 
                    leftBottomLocation.y, 
                    leftBottomLocation.z + scale * k + GRID_OFFSET
                    );

                if (!IsObstacleAtLocation(spawnPos))
                {
                    GameObject obj = Instantiate(gridPrefab, spawnPos, Quaternion.identity);
                    obj.transform.SetParent(gameObject.transform);
                    obj.GetComponent<Node>().x = i;
                    obj.GetComponent<Node>().y = k;

                    gridArray[i, k] = obj;
                }
                
            }
        }
    }


    bool IsObstacleAtLocation(Vector3 pos)
    {
        float radius = .5f * scale;

        Collider[] hitColliders = Physics.OverlapSphere(pos, radius);
        bool obstacle = false;

        //Check for obstacle. Do not place grid object if there is an obstacle here.
        foreach (Collider collider in hitColliders)
        {
            if(collider.gameObject.tag == "Obstacle")
            {
                obstacle = true;
                break;
            }
        }

        //Debug.Log("x: " + pos.x + " z: " + pos.z + " obstacle: " + obstacle);

        return obstacle;
    }

    void SetDistance()
    {
        InitialSetUp();
        int x = _startX;
        int y = _startY;
        int[] testArray = new int[rows * columns];

        for(int step = 1; step < rows * columns; step++)
        {
            foreach(GameObject obj in gridArray)
            {
                if(obj && obj.GetComponent<Node>().visited == step -1)
                {
                    TestFourDirections(obj.GetComponent<Node>().x, obj.GetComponent<Node>().y, step);
                }
            }
        }
    }

    void SetPath(int movementRange)
    {
        int step;
        int x = _endX;
        int y = _endY;
        List<GameObject> tempList = new List<GameObject>();
        List<GameObject> tempPath = new List<GameObject>();
        _path.Clear();

        if (gridArray[x,y] && gridArray[x, y].GetComponent<Node>().visited > 0)
        {
            
            step = gridArray[x, y].GetComponent<Node>().visited - 1;
            Debug.Log("step= " + step + ", range= " + movementRange);

            if(movementRange < 0)
                _path.Add(gridArray[x, y]);

        }
        else
        {
            print("Can't reach desired location");
            return;
        }

        for (int i = step; step > -1; step--)
        {

            if(TestDirection(x, y, step, 1))
                tempList.Add(gridArray[x, y + 1]);
            if (TestDirection(x, y, step, 2))
                tempList.Add(gridArray[x + 1, y]);
            if (TestDirection(x, y, step, 3))
                tempList.Add(gridArray[x, y - 1]);
            if (TestDirection(x, y, step, 4))
                tempList.Add(gridArray[x - 1, y]);

            GameObject tempObj = FindClosest(gridArray[_endX, _endY].transform, tempList);

            if((movementRange > -1 && step <= movementRange) || movementRange < 0)
            {
                _path.Add(tempObj);
            }
            
            x = tempObj.GetComponent<Node>().x;
            y = tempObj.GetComponent<Node>().y;

            //Debug.Log("Path Count!: " + _path.Count + ", Path Count x: " + x + ", Path Count y: " + y);
            tempList.Clear();
        }

        //_path = tempPath;

        //Debug.Log("Path Count!: " + _path.Count);
    }

    public List<GameObject> GetPath()
    {
        return _path;
    }


    public List<GameObject> GetSubPath(List<GameObject> list, int range)
    {
        Debug.Log("GetSubPath --> range: " + range + "count: " + list.Count);
        if (list.Count < range)
            return list;
        return list.GetRange(list.Count - range, range-1);
        //return _path;
    }

    void InitialSetUp()
    {
        foreach(GameObject obj in gridArray)
        {
            if(obj)
            {
                obj.GetComponent<Node>().visited = -1;
            }
            
        }
        gridArray[_startX, _startY].GetComponent<Node>().visited = 0;
    }

    bool TestDirection(int x, int y, int step, int direction)
    {
        //int directions tells us which case to use. 1 is up, 2 is right, 3 is down, 4 is left
        switch(direction)
        {
            case 4:
                if (x - 1 > -1 && gridArray[x - 1, y] && gridArray[x - 1, y].GetComponent<Node>().visited == step)
                    return true;
                else
                    return false;
             
            case 3:
                if (y - 1 > -1 && gridArray[x, y - 1] && gridArray[x, y - 1].GetComponent<Node>().visited == step)
                    return true;
                else
                    return false;
            
            case 2:
                if (x + 1 < columns && gridArray[x + 1, y] && gridArray[x + 1, y].GetComponent<Node>().visited == step)
                    return true;
                else
                    return false;
      
            case 1:
                if (y + 1 < rows && gridArray[x, y + 1] && gridArray[x, y + 1].GetComponent<Node>().visited == step)
                    return true;
                else
                    return false;
   
        }

        return false;
    }

    void TestFourDirections(int x, int y, int step)
    {
        if (TestDirection(x, y, -1, 1))
            SetVisited(x, y + 1, step);
        if (TestDirection(x, y, -1, 2))
            SetVisited(x + 1, y, step);
        if (TestDirection(x, y, -1, 3))
            SetVisited(x, y - 1, step);
        if (TestDirection(x, y, -1, 4))
            SetVisited(x - 1, y, step);
    }

    void SetVisited(int x, int y, int step)
    {
        if(gridArray[x,y])
        {
            gridArray[x, y].GetComponent<Node>().visited = step;
        }
    }

    GameObject FindClosest(Transform targetLocation, List<GameObject> list)
    {
        float currentDistance = scale * rows * columns;
        int indexNumber = 0;

        for(int i = 0; i < list.Count; i++)
        {
            if(Vector3.Distance(targetLocation.position, list[i].transform.position) < currentDistance)
            {
                currentDistance = Vector3.Distance(targetLocation.position, list[i].transform.position);
                indexNumber = i;
            }
        }

        return list[indexNumber];
    }

    public bool GetEnemiesInAttackRange(Node targetNode, Node enemyNode)
    {

        Node leftNode = GetNodeOrNull(targetNode.x - 1, targetNode.y);
        Node rightNode = GetNodeOrNull(targetNode.x + 1, targetNode.y);
        Node upNode = GetNodeOrNull(targetNode.x, targetNode.y + 1);
        Node downNode = GetNodeOrNull(targetNode.x, targetNode.y - 1);

        if (
            leftNode && leftNode == enemyNode
            || rightNode && rightNode == enemyNode
            || upNode && upNode == enemyNode
            || downNode && downNode  == enemyNode
            )
            return true;

        return false;
    }

    public Node GetNodeOrNull(int x, int y)
    {
        if (x < 0 || x > rows-1 || y < 0 || y > columns-1) //invalid
        {
            return null;
        }
        return gridArray[x, y].GetComponent<Node>();
    }
}
