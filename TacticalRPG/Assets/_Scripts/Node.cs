﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    [SerializeField]
    Material[] materials;

    public int visited = -1;
    public int x = 0;
    public int y = 0;

    private const string PLAYER_RANGE_TAG = "PlayerRange";
    private const string AI_RANGE_TAG = "AIRange";

    private const int MAT_NORMAL = 0;
    private const int MAT_PLAYER_RANGE = 1;
    private const int MAT_MOUSE_HIGHLIGHT = 2;
    private const int MAT_AI_RANGE = 3;
    private const int MAT_ATTACK_RANGE = 4;

    bool shouldShowRange, isMouseHovering, canMoveToo;
    List<string> colliderTagList = new List<string>();

    GridBehavior gridBehavior;

    // Start is called before the first frame update
    void Start()
    {
        shouldShowRange = true;
        isMouseHovering = false;
        canMoveToo = false;
        gameObject.GetComponent<MeshRenderer>().material = materials[MAT_NORMAL];
        gridBehavior = gameObject.GetComponentInParent<GridBehavior>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnMouseEnter()
    {
        gameObject.GetComponent<MeshRenderer>().material = materials[MAT_MOUSE_HIGHLIGHT];

        isMouseHovering = true;
    }

    void OnMouseExit()
    {
        gameObject.GetComponent<MeshRenderer>().material = materials[MAT_NORMAL];

        isMouseHovering = false;
    }

    void OnMouseDown()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && canMoveToo) //left click
        {
            /* gridBehavior.SetDestination(
                 gameObject.GetComponent<GridStats>().x, 
                 gameObject.GetComponent<GridStats>().y
                 ); */
            GameController.ProcessEvent(GameController.GameEvent.NODE_CLICKED, gameObject);
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == PLAYER_RANGE_TAG)
        {
            canMoveToo = true;
            if (shouldShowRange && !isMouseHovering)
            {
                gameObject.GetComponent<MeshRenderer>().material = materials[MAT_PLAYER_RANGE];
            }
            if (!colliderTagList.Contains(PLAYER_RANGE_TAG))
            {
                colliderTagList.Add(PLAYER_RANGE_TAG);
            }
        } 
        else if (other.gameObject.tag == AI_RANGE_TAG)
        {
            if (shouldShowRange && !isMouseHovering)
            {
                gameObject.GetComponent<MeshRenderer>().material = materials[MAT_AI_RANGE];
            }
            if (!colliderTagList.Contains(AI_RANGE_TAG))
            {
                colliderTagList.Add(AI_RANGE_TAG);
            }
        }       
            
        if (colliderTagList.Contains(PLAYER_RANGE_TAG) 
            && colliderTagList.Contains(AI_RANGE_TAG) 
            && shouldShowRange 
            && !isMouseHovering)
        {
            gameObject.GetComponent<MeshRenderer>().material = materials[MAT_ATTACK_RANGE];
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == PLAYER_RANGE_TAG)
        {
            canMoveToo = false;
            colliderTagList.Remove(PLAYER_RANGE_TAG);
        }
        else if (other.gameObject.tag == AI_RANGE_TAG)
        {
            colliderTagList.Remove(AI_RANGE_TAG);
        }
        gameObject.GetComponent<MeshRenderer>().material = materials[MAT_NORMAL];
    }


    public bool CanMoveToo()
    {
        return canMoveToo;
    }
}
