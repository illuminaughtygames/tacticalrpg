﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    List<GameObject> _path = new List<GameObject>();
    public Node 
        currentNode,
        lastNode;

    const string IDLE_ANIM_KEY = "isIdle";
    const string MOVING_ANIM_KEY = "isMoving";
    public int movementSpeed = 3;

 
    void Start()
    {
        movementSpeed = 3;
    }

 
    void Update()
    {
        //Player State: Moving
        if (_path != null && _path.Count > 0)
        {
            gameObject.GetComponent<Animator>().SetBool(IDLE_ANIM_KEY, false);
            gameObject.GetComponent<Animator>().SetBool(MOVING_ANIM_KEY, true);

            GameObject target = _path[_path.Count - 1];
            Vector3 nextPos = new Vector3(
                target.GetComponent<Node>().x + GridBehavior.GRID_OFFSET, 
                0, 
                target.GetComponent<Node>().y + GridBehavior.GRID_OFFSET
                );

            //Debug.Log("nextPos: " + nextPos + "path count: " + _path.Count);

            if (IsAtTarget(target))
            {
                _path.Remove(target);

                if(_path.Count < 1) //walked all of the path
                {
                    _path = null;
                    Debug.Log("_path set to null");
                    GameController.ProcessEvent(GameController.GameEvent.ACTION_DONE, target);
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, nextPos, 5f * Time.deltaTime);

                if(!IsAtTarget(target))
                {
                    Vector3 direction = (nextPos - transform.position).normalized;
                    Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);

                    transform.rotation = rotation;
                }               
            }
        }
        //Player State: Idle
        else
        {
            gameObject.GetComponent<Animator>().SetBool(MOVING_ANIM_KEY, false);
            gameObject.GetComponent<Animator>().SetBool(IDLE_ANIM_KEY, true);
        }
    }

    public void SetPath(List<GameObject> path)
    {
        _path = path;
    }

    public void MoveToPosition(Vector3 vector)
    {
        transform.position = vector;
        _path = null;
    }

    bool IsAtTarget(GameObject target)
    {
        return transform.position.x == target.GetComponent<Node>().x + GridBehavior.GRID_OFFSET
                && transform.position.z == target.GetComponent<Node>().y + GridBehavior.GRID_OFFSET;
    }

    void AlertObservers(string message)
    {
        if (message.Equals("AnimationEnd"))
        {
            GameController.ProcessEvent(GameController.GameEvent.ACTION_DONE, null);
        }
    }


    public void DecideMove()
    {
        StartCoroutine("AiMove");
    }

    public void DecideEndTurn()
    {
        StartCoroutine("AiEndTurn");
    }

    IEnumerator AiMove()
    {
        yield return new WaitForSeconds(2);
        GameController.ProcessEvent(GameController.GameEvent.NODE_CLICKED, null);
    }

    IEnumerator AiEndTurn()
    {
        yield return new WaitForSeconds(2);
        GameController.ProcessEvent(GameController.GameEvent.END_TURN, null);
    }
}
