﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserActionMenu : MonoBehaviour
{
   public void Attack()
    {
        GameController.ProcessEvent(GameController.GameEvent.ATTACK, null);
        gameObject.SetActive(false);
    }

    public void UseItem()
    {
        GameController.ProcessEvent(GameController.GameEvent.USE_ITEM, null);
        gameObject.SetActive(false);
    }

    public void Undo()
    {
        GameController.ProcessEvent(GameController.GameEvent.UNDO, null);
        gameObject.SetActive(false);
    }
    public void EndTurn()
    {
        GameController.ProcessEvent(GameController.GameEvent.END_TURN, null);
        gameObject.SetActive(false);
    }

    public void CanAttack(bool canAttack)
    {
        gameObject.transform.Find("AttackButton")
            .gameObject.SetActive(canAttack);
    }
}
